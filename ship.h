#ifndef SHIP_H_INCLUDED
#define SHIP_H_INCLUDED
#include "global.h"
#include <stdio.h>
#include <string.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
class Ship
{
public:
    Ship();
    void Move(int x , int y);
    int getPositionX()
    {
        return positionX;
    }
    int getPositionY()
    {
        return positionY;
    }
    bool dead();
    void subtractHP(int x);
    void Draw(); //redraw position
    int HP;
private:
    bool dir_right;
    int positionX;
    int positionY;

    ALLEGRO_BITMAP* img_forward;
    ALLEGRO_BITMAP* img_backward;
};

#endif // SHIP_H_INCLUDED
