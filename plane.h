#ifndef PLANE_H_INCLUDED
#define PLANE_H_INCLUDED
#include "global.h"
#include <stdio.h>
#include <string.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include<iostream>

using namespace std;

class Plane
{
public:
    Plane(int x , int y);
    ~Plane();
    int getPositionX()
    {
        return positionX;
    }
    int getPositionY()
    {
        return positionY;
    }
    void dead()
    {
        positionX = 0;
        positionY = 0;
    }
    void setPosX(int x);
    void Move();
    void Draw();
    bool redraw = true;
private:
    int positionX;
    int positionY;
    int velocityX , velocityY;
    int HP;
    ALLEGRO_BITMAP* img;
};


#endif // PLANE_H_INCLUDED
