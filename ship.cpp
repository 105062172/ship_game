#include"ship.h"
#include"global.h"
#include<iostream>

using namespace std;

Ship::Ship()
{
    img_backward = al_load_bitmap("./Battleship.png");
    img_forward = al_load_bitmap("./Battleship_forward.png");
    dir_right = true;
    positionX = 0;
    positionY = 500;
    HP = 100;
}
bool Ship::dead()
{
    if(HP<=0)
        return true;
    else
        return false;
}
void Ship::subtractHP(int x)
{
    HP -= x;
}
void Ship::Move(int x , int y)
{
    if(x > 0)
        dir_right = true;
    else if(x == 0)
        dir_right = dir_right;
    else
        dir_right = false;
    cout <<"Move" << endl;
    positionX += x;
    positionY += y;
}

void Ship::Draw()
{
    if(dir_right)
        al_draw_bitmap(img_forward , positionX , positionY , 0);
    else
        al_draw_bitmap(img_backward , positionX , positionY , 0);
    cout << positionX << "  " << positionY << endl;
}
