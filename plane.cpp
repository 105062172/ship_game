#include"plane.h"


Plane::Plane(int x , int y)
{
    positionX = 0;
    positionY = 0;
    velocityX = x;
    velocityY = y;
    HP = 100;
    img =al_load_bitmap("./Battleship.png");
}
Plane::~Plane()
{
    al_destroy_bitmap(img);
}
void Plane::setPosX(int x)
{
    positionX = x;
}
void Plane::Move()
{
    positionX += velocityX;
    positionY += velocityY;
}
void Plane::Draw()
{
    al_draw_bitmap(img , positionX , positionY , 0);
}
