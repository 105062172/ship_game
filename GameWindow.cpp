#include "GameWindow.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
using namespace std;


bool isOverlap(int x1, int y1 , int x2 , int y2)
{
    if(x1 - x2<=50 && x1-x2>=-50 && y1 - y2<=50 && y1-y2>=-50)
        return true;
    else
        return false;
}



GameWindow::GameWindow()
{
    UP = false;
    DOWN = false;
    RIGHT = false;
    LEFT = false;
    times = 0;

     if (!al_init())
        show_err_msg(-1);

    printf("Game Initializing...\n");

    display = al_create_display(window_width, window_height);
    event_queue = al_create_event_queue();
    timer = al_create_timer(1.0 / FPS);


    if (display == NULL)
        show_err_msg(-1);
    if (timer == NULL)
        show_err_msg(-1);


    al_init_primitives_addon();
    al_init_image_addon();
    al_init_acodec_addon();

    al_install_keyboard(); // install keyboard event
    al_install_mouse();    // install mouse event
}
void GameWindow::game_init()
{

    srand(time(NULL));
    icon = al_load_bitmap("./icon.jpg");
    background = al_load_bitmap("./Background.png");
    al_set_display_icon(display, icon);

    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_timer_event_source(timer));

    player = new Ship();
    //push plane
    for(int i = 0 ;i <= 10;i++)
    {
        Plane *p = new Plane( (rand()%5)*(-1), 3);
        p->setPosX(rand()%700+100);
        planeSet.push_back(p);
    }

}
void GameWindow::game_begin()
{
    al_clear_to_color(al_map_rgb(100,100,100));
    al_draw_bitmap(background, 0, 0, 0);
    al_start_timer(timer);
    player->Draw();
    al_flip_display();
}
void GameWindow::game_destroy()
{
    al_destroy_event_queue(event_queue);
    al_destroy_display(display);
}

int GameWindow::game_run()
{
    int error = 0;
    if (!al_is_event_queue_empty(event_queue)) {
        error = process_event();

    }
    return error;
}

int GameWindow::process_event()
{
    times++;
    al_wait_for_event(event_queue, &event);
    if(times > 350)
        return GAME_TERMINATE;
    if(event.type == ALLEGRO_EVENT_TIMER)
    {
        if(event.timer.source == timer)
        {
            redraw();
        }
    }
    else if(event.type == ALLEGRO_EVENT_KEY_DOWN)
    {
        //if(event.keyboard.keycode == )
        switch(event.keyboard.keycode)
        {
            case ALLEGRO_KEY_W:
                UP = true;
                break;
            case ALLEGRO_KEY_S:
                DOWN = true;
                break;
            case ALLEGRO_KEY_D:
                RIGHT = true;
                break;
            case ALLEGRO_KEY_A:
                LEFT = true;
                break;
        }
    }
    else if(event.type == ALLEGRO_EVENT_KEY_UP)
    {
        switch(event.keyboard.keycode)
        {
            case ALLEGRO_KEY_W:
                UP = false;
                break;
            case ALLEGRO_KEY_S:
                DOWN = false;
                break;
            case ALLEGRO_KEY_D:
                RIGHT = false;
                break;
            case ALLEGRO_KEY_A:
                LEFT = false;
                break;
        }
    }
    else if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
    {
        return GAME_TERMINATE;
    }
    if(UP)
        player->Move(0 , -5);
    if(DOWN)
        player->Move(0 , 5);
    if(RIGHT)
        player->Move(5 , 0);
    if(LEFT)
        player->Move(-5 , 0);
    redraw();
    cout << times<<endl;

}
void GameWindow::redraw()
{
    al_clear_to_color(al_map_rgb(100,100,100));
    al_draw_bitmap(background, 0, 0, 0);
    if(times>=20)
    {
        if(planeSet[0])
        {
            planeSet[0]->Move();
            planeSet[0]->Draw();
        }

    }
    if(times>=50)
    {
        if(planeSet[1]->redraw)
        {
            planeSet[1]->Move();
            planeSet[1]->Draw();
        }
    }
    if(times>=60)
    {
        if(planeSet[2]->redraw)
        {
            planeSet[2]->Move();
            planeSet[2]->Draw();
        }
    }
    if(times>=95)
    {
        if(planeSet[3]->redraw)
        {
            planeSet[3]->Move();
            planeSet[3]->Draw();
        }
    }
    if(times>=125)
    {
        if(planeSet[4]->redraw)
        {
            planeSet[4]->Move();
            planeSet[4]->Draw();
        }

    }
    if(times>=150)
    {
        if(planeSet[5]->redraw)
        {
            planeSet[5]->Move();
            planeSet[5]->Draw();
        }
    }
    if(times>=165)
    {
        if(planeSet[6]->redraw)
        {
            planeSet[6]->Move();
            planeSet[6]->Draw();
        }
    }
    if(times>=195)
    {
        if(planeSet[7]->redraw)
        {
            planeSet[7]->Move();
            planeSet[7]->Draw();
        }
    }
    if(times>=205)
    {
        if(planeSet[8]->redraw)
        {
            planeSet[8]->Move();
            planeSet[8]->Draw();
        }
    }
    if(times>=225)
    {
        if(planeSet[9]->redraw)
        {
            planeSet[9]->Move();
            planeSet[9]->Draw();
        }
    }
    for(int i = 0;i< 10;i++)
    {
        if(isOverlap(player->getPositionX(), player->getPositionY()
                      , planeSet[i]->getPositionX() , planeSet[i]->getPositionY()))
        {
            cout << "true"<<endl;
            planeSet[i]->redraw = false;
            planeSet[i]->dead();
            player->subtractHP(50);
            cout << player->HP<<endl;
            if(player->dead())
                system("pause");
        }
    }

    player->Draw();
    al_flip_display();

}





void GameWindow::show_err_msg(int msg)
{
    if(msg == GAME_TERMINATE)
        fprintf(stderr, "Game Terminated...");
    else
        fprintf(stderr, "unexpected msg: %d", msg);

    game_destroy();
    exit(9);
}
