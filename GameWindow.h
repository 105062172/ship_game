#ifndef GAMEWINDOW_H_INCLUDED
#define GAMEWINDOW_H_INCLUDED


#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_image.h>
#include <vector>

#include"global.h"
#include"ship.h"
#include"plane.h"
#define GAME_INIT -1
#define GAME_SETTING 0
#define GAME_SELECT 1
#define GAME_BEGIN 2
#define GAME_CONTINUE 3
#define GAME_FAIL 4
#define GAME_TERMINATE 5
#define GAME_NEXT_LEVEL 6
#define GAME_EXIT 7
const float FPS = 35;
class GameWindow
{
public:
    GameWindow();
    void game_init();
    void game_begin();

    int game_run();//call process_event to run game

    int process_event();//wait for event

    void redraw();
    void show_err_msg(int msg);
    void game_destroy();
private:

    bool UP , DOWN , RIGHT , LEFT = false;
    int times;
    Ship *player;
    vector<Plane*> planeSet;
    ALLEGRO_BITMAP *icon;
    ALLEGRO_BITMAP *background = NULL;
    ALLEGRO_DISPLAY* display = NULL;
    ALLEGRO_TIMER *timer = NULL;

    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_EVENT event;
};
#endif // GAMEWINDOW_H_INCLUDED
