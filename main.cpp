#include "GameWindow.h"

int main(int argc, char *argv[])
{
    int msg;
    GameWindow *TowerGame = new GameWindow();

    TowerGame->game_init();
    TowerGame->game_begin();

    while (msg != GAME_TERMINATE) {
        msg = TowerGame->game_run();
        if (msg == GAME_TERMINATE)
            printf("See you, world\n");
    }

    TowerGame->game_destroy();
    delete TowerGame;
    return 0;
}
